import unittest
import func
# from HTMLTestRunner import HTMLTestRunner

class TestMath(unittest.TestCase):
    def test_add(self):
        self.assertEqual(2,func.add(1,1))
        self.assertEqual(2,func.add(2,0))

    def test_sub(self):
        self.assertEqual(1,func.sub(2,1))
        self.assertEqual(2,func.sub(2,0))

    def test_multi(self):
        self.assertEqual(4,func.multiple(1,4))
        self.assertEqual(4,func.multiple(2,2))

    i='tags'
    @unittest.skipIf(i=="tag","skip this case")
    def test_divi(self):
        self.assertEqual(2,func.divi(4,2))
        self.assertEqual(2,func.divi(6,0))

    def test_set(self):
        self.assertSetEqual(set([1,2,3,3]),set([1,3,2,2]))

    def test_list(self):
        self.assertListEqual([1,2,3],[1,3,2])
