# coding:utf-8
import unittest
import sys
import os

from common.uiCommon import Browser

reload(sys)
sys.setdefaultencoding('utf8')

marketingurl='http://www.huiyan.fzdzyun.com/group/guest/marketing'
marketingtitle='营销支撑 - 慧眼 选题策划与营销支撑'
tag='TBD'

class TestLogin(unittest.TestCase):

    @unittest.skipIf(tag=='TBD','skip this case')
    def test_login(self):
        browser=Browser()
        browser.login('yigl','123456')

    @unittest.skipIf(tag=='TBD','skip this case')
    def test_go_to_marketing_page(self):
        browser=Browser()
        browser.login_autotest_and_go_to(marketingurl,marketingtitle)

if __name__ == '__main__':
    unittest.main()

