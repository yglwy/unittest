import json
import unittest,jsonpath
from ddt import ddt,data,unpack
from tools.MyRequest import MyRequest
from tools.OperateExcel import GetTestData,WriteContentInOldBook

FILENAME = 'user.xlsx'
d = GetTestData(FILENAME,'Sheet1')
datas = d.get_data()

@ddt
class TestAddFold(unittest.TestCase):

    results = []

    @classmethod
    def setUpClass(cls):
        result = MyRequest('http://api.nnzhp.cn/api/user/login', method='post', data ={"username":"niuhanyang","passwd":"aA123456"})
        cls.cookie = 'niuhanyang=' + str(jsonpath.jsonpath(result.response, '$..sign')[0])

    @data(*datas)
    @unpack
    def test_add_fold(self, url, method, data, check):
        rd = []
        gold = MyRequest(url, method=method, headers={"cookie":self.cookie}, data = data)
        rd.append(gold.response)
        if jsonpath.jsonpath(gold.response, '$..msg')[0] == check['msg']:
            r = True
        else:
            r = False
        rd.append(r)
        self.results.append(rd)
        self.assertTrue(r)


    @classmethod
    def tearDownClass(cls):
        eb = WriteContentInOldBook(FILENAME, 0)
        for n,result in enumerate(cls.results):
            eb.write_content(n+1, 6, str(result[1]))
            eb.write_content(n+1, 7, json.dumps(result[0], ensure_ascii=False))
        eb.save_data()



unittest.main()

