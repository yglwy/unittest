# coding:utf-8
import time

def add_time():
    current_time=time.localtime()
    print current_time
    current_time.tm_sec=current_time.tm_sec+15
    before_time=time.ctime(current_time)
    return before_time

def get_current_time():
    current_time=time.localtime()
    return current_time

def time_difference(start,end):
    return time.ctime(end-start)


if __name__ == '__main__':
    start=time.time()
    print start
    time.sleep(1)
    end=time.time()
    print add_time()
    print get_current_time()
    print time_difference()