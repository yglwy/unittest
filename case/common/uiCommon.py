# coding:utf-8
from selenium import webdriver
import unittest
import sys
# import common
reload(sys)
sys.setdefaultencoding('utf8')

#url
loginurl='http://www.huiyan.fzdzyun.com/'
loginedurl='http://www.huiyan.fzdzyun.com/group/guest/'
publicsentimenturl='http://www.huiyan.fzdzyun.com/group/guest/newpublicsentiment'
marketingurl='http://www.huiyan.fzdzyun.com/group/guest/marketing'

#title
loginedtitle='行业舆情 - 慧眼 选题策划与营销支撑'
marketingtitle='营销支撑 - 慧眼 选题策划与营销支撑'

#content
subscriblepagecontent='图书订阅'

#element
idcommon='_com_founder_portal_pbd_security_LoginPortlet_'
usernametextid=idcommon+'login'
pwdtextid=idcommon+'password'
loginbtnid=idcommon+'loginBtn'
subscriblebtnid='_com_founder_portal_pbd_portlet_MarketingPortlet_subscribeBookBtn'

class Browser(unittest.TestCase):
    def __init__(self):
        self.browser = webdriver.Chrome()
        self.browser.maximize_window()

    def close_browser(self):
        self.browser.quit()

    def send_keys_by_element_id(self,elementid,value):
        self.browser.find_element_by_id(elementid).send_keys(value)

    def click_btn_by_element_id(self,elementid):
        self.browser.find_element_by_id(elementid).click()

    def go_to_nav_page(self,url,title):
        self.browser.get(url)
        self.browser.implicitly_wait(30)
        self.page_title_should_be(url,title)

    def page_title_should_be(self,url,loginedtitle):
        currenturl = self.browser.current_url
        print currenturl
        pageTitle=self.browser.title
        print pageTitle
        self.assertIn(url,currenturl)
        self.assertEqual(pageTitle,loginedtitle)

    def page_should_contain(self,text):
        contents=self.browser.page_source
        self.assertIn(text,contents.encode('utf-8'))

    def login(self,username,pwd):
        self.browser.get(loginurl)
        self.send_keys_by_element_id(usernametextid,username)
        self.send_keys_by_element_id(pwdtextid,pwd)
        self.click_btn_by_element_id(loginbtnid)
        self.browser.implicitly_wait(30)
        self.page_title_should_be(loginedurl,loginedtitle)

    def login_autotest_and_go_to(self,url,title):
        self.login('autotest','123456')
        self.go_to_nav_page(url,title)

    def login_autotest_and_go_to_marketing(self):
        self.login_autotest_and_go_to(marketingurl,marketingtitle)

    def open_subscribe_book_page(self):
        self.click_btn_by_element_id(subscriblebtnid)
        self.browser.implicitly_wait(30)
        self.page_should_contain(subscriblepagecontent)

    def subscribe_book(self):
        self.open_subscribe_book_page()

