# coding:utf-8
import unittest
# from HTMLTestRunner import HTMLTestRunner
from  BeautifulReport import BeautifulReport
import os

case_path=os.path.join(os.getcwd(),'case')
report_path=os.path.join(os.getcwd(),'report')
print(report_path)

def all_case():
    discover=unittest.defaultTestLoader.discover(case_path,pattern="test*.py",top_level_dir=None)
    return discover

def mkdir(path):
    # 去除首位空格
    path=path.strip()
    # 去除尾部 \ 符号
    path=path.rstrip("\\")
    isExists=os.path.exists(path)
    if not isExists:
        os.makedirs(path)
        # print(path+' 创建成功')
        return True
    else:
        # print(path+' 目录已存在')
        return False


if __name__ == '__main__':
    mkdir(report_path)
    result = BeautifulReport(all_case())
    result.report(filename='单元测试报告',description='测试函数',report_dir='report', theme='theme_default')


