# coding:utf-8
import unittest
import apiCommon
import json

class TestRequest(unittest.TestCase):
    def test_post(self):
        url='http://pub.prodb.test.fzyun.io/sys/orgs'
        para,r=apiCommon.sysPost(url)
        self.assertEqual(r.status_code,200)
        response=json.loads(r.text)
        self.assertEqual(response['account'],para['account'])

    def test_get(self):
        url='http://pub.prodb.test.fzyun.io/sys/orgs'
        para,r=apiCommon.sysPost(url)
        self.assertEqual(r.status_code,200)
        response=json.loads(r.text)
        ids=response['id']
        r=apiCommon.sysGet(url,ids)
        self.assertEqual(r.status_code,200)

if __name__ =='__main__':
    unittest.main()

