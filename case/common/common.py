# coding:utf-8
import time
import random

def get_local_time_pattern_data():
    times=time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))
    return times

def get_local_time_pattern_date():
    times=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    return times

def get_random_data(mindata,maxdata):
    data=random.randint(mindata,maxdata)
    return data